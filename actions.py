# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

import random
from typing import Any, Text, Dict, List
import csv

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

# from pymongo import MongoClient
# from myTracker import insert_logs


# client = MongoClient()
# client = MongoClient('mongodb://localhost:27017/')
# db = client['NAMS_logs']
# collection = db['nams_collection']


# class ActionDefaultAskAffirmation(Action):
#     def name(self):
#         return "action_default_ask_affirmation"

#     def __init__(self):
#         self.intent_mappings = {}
#         with open('data/intent_mapping.csv', newline='',encoding='utf-8') as file:
#             csv_reader = csv.reader(file)
#             for row in csv_reader:
#                 self.intent_mappings[row[0]] = row[1]

#     def run(self, dispatcher, tracker, domain):
#         last_intent_name = tracker.latest_message['intent']['name']
#         intent_prompt = self.intent_mappings[last_intent_name]
#         message = f"Did you mean '{intent_prompt}'?"
#         buttons = [{'title':'Yes',
#                     'payload':f'/{last_intent_name}'},
#                     {'title':'No',
#                     'payload':'/action_default_ask_rephrase'}]
#         dispatcher.utter_message(text=message,buttons=buttons)
#         return []


# class ActionDefaultAskRephrase(Action):
#     def name(self):
#         return "action_default_ask_rephrase"

#     def run(self,dispatcher,tracker,domain):
#         message = "Can you repeat your question more clearly?"
#         dispatcher.utter_message(message)
#         return []


class Greeting(Action):
    def name(self) -> Text:
        return "action_greeting"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        reply = ["Hello 👋, this is NAMS (Nepal Audit Management System) chatbot. How can I assit you?",
                 "Welcome 🙏 to NAMS chatbot. How can I help you?",
                 "Namaste 🙏. This is NAMS (Nepal Audit Management System) chatbot. I can help you to solve your doubts regarding the our services or activities."]
        message = random.choice(reply)
        dispatcher.utter_message(message)

        # # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class AfterGreet(Action):
    def name(self) -> Text:
        return "action_after_greet"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        reply = ["I am doing very well.",
                 "I am doing perfectly fine.",
                 "I am feeling very great today."]
        message = random.choice(reply)
        dispatcher.utter_message(message)

        # # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class ProfaneWords(Action):
    def name(self) -> Text:
        return "action_profane_words"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any, ]) -> List[Dict[Text, Any]]:

        reply = ["🤬 Such words are not accepted.",
                 "🤬. We cannot accept such words from you.",
                 "🤬 Please do not offend me."]

        message = random.choice(reply)
        dispatcher.utter_message(message)

        # # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class ActionOutOfScope(Action):
    def name(self) -> Text:
        return "action_out_of_scope"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        reply = ["I am sorry, but it seems your message is quite diffcult to understand. 😕",
                 "Sorry. 😕 Your message is not understandable or seems to be out of my scope"]
        message = random.choice(reply)
        dispatcher.utter_message(message)

        # # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class Goodbye(Action):
    def name(self) -> Text:
        return "action_goodbye"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any, ]) -> List[Dict[Text, Any]]:

        reply = ["👋 Thank you for using NAMS chatbot.",
                 "👋 I am very happy you used this chatbot. You are always welcome here."]
        message = random.choice(reply)
        dispatcher.utter_message(message)

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class AboutInfinity(Action):
    def name(self) -> Text:
        return "action_about_nams_bot"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:
        reply = ["This chatbot is developed to help you in clearing your doubts regarding our services.",
                 "This is a chabot of NAMIS, designed to help you with your services in a more efficient way."]
        message = random.choice(reply)
        dispatcher.utter_message(message)

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class AskDefinitions(Action):
    def name(self) -> Text:
        return "action_ask_definitions"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        switch = {
            "Alphanumeric Data": '''These are textual data that includes name, description, and any other data containing text.''',
            "Audit Area": '''Audit is conducted on some subject matters called audit areas which vary over
                            the audit steps, audit risks and internal control mechanism.\nFor example, audit
                            areas are Pay & Allowances, Construction & Development, Procurement, Inventory, Works Expenditure etc.''',
            "Audit Execution Year": '''This refers to the financial year when audit is taken place in the entity.''',
            "Audit Observation": '''This is the irregularity identified by the audit team during audit execution.''',
            "Audit Program": '''Audit Program is a set of audit steps(with guideline, risks and procedures) of an audit process
                            that must be followed and checked during audit execution with working paper references.''',
            "Auditable Year": '''This refers to the years to be audited.''',
            "Authorized": '''Authorized refers to the acesss right to the features of the system by the user through the
                            roles of the user. Authorized are combined into a certain role.''',
            "Entity preparing financial statement": '''Entities preparing financial statement (EPFS) are entities which prepare
                            consolidated/standalone financial statements as per NFRS or NPSAS and where OAGN is mandated to provide audit
                            opinion as per PARF.''',
            "Entity": '''The offices such as ministries, departments, sub-ordinate offices, branch offices etc. to be audited are called
                        entity or also called auditable unit''',
            "Entity Owner Unit": '''Every year teams consisting of audit officers and auditors are created, which can be called the Entity Owner Unit.
                                This team is responsible for audit reporting, follow-ups and other administrative works w.r.t an entity assigned to the team.''',
            "Entrance Meeting": '''It is the first meeting among the concerned personnel of entity and audit team members prior to start of the
                                audit in the entity.''',
            "Exit Meeting:": '''On the final day of audit, the audit team meets with the head (or concerned personnel) of the entity to discuss about the
                            identified audit observations. This meeting is called the Exit meeting.''',
            "Field Audit Unit": '''It is the audit team which consist of Team Leader and Team Members for conducting field visit and audit of an entity.''',
            "Master Data Management": '''These are the data, that are included in internal logic for functioning feature of the system. Master data
                            rarely change and are not allowed to remove from the system.''',
            "Numeric Data": '''These are number data that includes budget amount, expenditure amount, age, year of experience etc.''',
            "Preliminary Audit Report": '''Preliminary Audit Report (PAR) is the report that contains audit observations with their details
                                collected during audit execution. The team leader submits this report to the head or concerned personnel of the entity
                                after completion of the audit.''',
            "Risk Grade of Entity": '''There are three grades A, B and C. The grades A/B/C are defined for every entity in every year based on
                            associated risk score where risks are determined by last year's Materiality (total budget and capital budget),
                            Sensitivity (irregular amount) and other criteria.''',
            "Role": '''Role is a set of privileges that together form the rights for the users and authenticate the users to access and operate the
                    features and modules of the system.''',
            "User": '''User is anybody using NAMS System.''',
        }
        entity = tracker.latest_message['entities']
        message = switch.get(entity)
        if message:
            dispatcher.utter_message(message)
        else:
            dispatcher.utter_message(
                "Sorry I could not get what you asked for.")

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class AskAbbreviations(Action):
    def name(self) -> Text:
        return "action_ask_abbreviations"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        switch = {
            "AAG": "Assistant Auditor General",
            "AAP": "Annual Audit Plan",
            "AR": "Annual Report",
            "AG": "Auditor General",
            "AI": "Artificial Intelligence",
            "ASYCUDA": "Automated System for Customer Data",
            "CA": "Compliance Audit",
            "CGAS": "Computerized Government Account System",
            "CR": "Change Request",
            "DAG": "Deputy Auditor General",
            "DC": "Data Center",
            "DFR": "Draft Final Report",
            "DMS": "Debt Management System",
            "EA": "Enterprise Architecture",
            "EAP": "Entity Level Plan",
            "EPFS": "Entity Preparining Financial Statement",
            "FA": "Financial Audit",
            "FS": "Financial Statement",
            "FCGO": "Financial Comptroller General Office",
            "GoN": "Government of Nepal",
            "ITS": "Integrated Tax System",
            "LMBID": "Line Ministry Budget Information System",
            "MAP": "Ministry Level Plan",
            "NAMS": "Nepal Audit Management System",
            "NFRS": "Nepal Financial Reporting Standards",
            "NGeA": "Nepal Government Enterprise Architecture",
            "NLP": "Natural Language Processing",
            "NPSAS": "Nepal Public Sector Accounting Standards",
            "OCR": "Optical Character Reader",
            "O&M": "Organization and Management",
            "OAGN": "Office of the Auditor General Nepal",
            "PA": "Performance Audit",
            "PAC": " Public Account Committee",
            "PARF": "Public Audit Restructuring Framework",
            "PIS": "Personal Information System",
            "QA": "Quality Assurance",
            "QC": "Quality Control",
            "RBAF": "Risk Based Audit Framework",
            "RMIS": "Revenue Management Information System",
            "SOE": "State Own Enterprises",
            "SUTRA": "Sub-national Treasury Regulatory Application",
            "TL": "Team Leader",
            "TM": "Team Member",
            "TSA": "Treasury Single Account ",
            "PAR": "Preliminary Audit Report",
        }
        entity = tracker.latest_message['entities']
        message = switch.get(entity)
        if message:
            dispatcher.utter_message(message)
        else:
            dispatcher.utter_message(
                "Sorry I could not get what you asked for.")

        # # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class ProjectBackgroud(Action):
    def name(self) -> Text:
        return "action_project_background"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        message = '''
        Strengthening of institutional systems and capacity of the Office of Auditor General of Nepal (OAGN) 
        by enhancement of quality of Audit works through improvement of audit methodology, effective use of IT 
        system and citizen engagement in public audit is one of the key objectives of the Government of Nepal’s 
        Public Financial Management Reform Strategy/Program (PFMRP) Phase II (2016/17-2025/26). \n
        Under this strategic plan, OAGN has come up with a project for “Strengthening of Office of Auditor General of Nepal”. 
        Component two of the project focus on “Deepening usage of Nepal Audit Management System (NAMS) by either 
        updating it or developing a new system”.
        '''
        dispatcher.utter_message(message)

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class ProjectExpectation(Action):
    def name(self) -> Text:
        return "action_project_expectation"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        message = '''
        OAGN has following key expectations from the project: \n
        • The main expected outcome is a usable NAMS (Nepal Audit Management System), which supports entire audit activities in digital mode leveraging ICT and AI based applications. \n
        • NAMS shall support risk-based audit planning, preparation of annual audit plan, audit execution, audit follow-up and audit reporting processes. It shall also support issuing audit queries, audit templates, responses, preparation of draft and final audit reports, and inventory of audit paras. \n
        • NAMS shall support audit in context of new federal structure.
        '''
        dispatcher.utter_message(message)

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class FeaturesDAnams(Action):
    def name(self) -> Text:
        return "action_feature_da_nams"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        message = '''
        The list of the features that are found in the report of Diagnostic Assessment of NAMS are: -\n
        •	Workflow based web application \n 
        •	Role based decision support dashboards \n
        •	System based follow-up on time critical actions through automated e-mail & SMS \n
        •	Signing of electronic documents using Digital Signature Certificate \n
        •	Standard audit report template for all types of Audit \n
        •	Online document management with intelligent search mechanism \n
        •	Effective communication through mobile application \n
        •	Complete audit life cycle management (Plan-Execute-Report-Compliance-Monitor) \n
        •	2 Tier Risk Based Approach (RBA) \n
        •	System based optimal route planning and audit calendar using MAP APIs \n
        •	Automated audit execution checklist and template-based audit para drafting \n
        •	Grievance management with configurable redressal mechanism \n
        •	Integrated with external applications  Multilingual support (English and Nepali) \n
        •	E-Manual with linking to each module and context sensitive search facility \n
        •	Person wise accountability \n
        •	Open Source Development \n
        •	Linkage with Audit Tools and Analytical Tools \n
        •	Automatic generation of audit trail to audit reports \n
        •	Compliance to National Enterprise Architecture (Nepal) \n
        •	Confirm to INTOSAI
        '''
        dispatcher.utter_message(message)

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class ProjectCommitteeMember(Action):
    def name(self) -> Text:
        return "action_committee_members"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        message = '''
        The committee that was formed by OAGN for the purpose of requirement gathering has the following 5 members: \n
        •	Mr. Narayan Parajuli, Director \n
        •	Mr. Chandra Kanta Bhnadari, Director \n
        •	Mr. Kamal Prasad Silwal, Director (Specialist) \n
        •	Mr. Ramesh Raj Subedi, Director (IT) \n
        •	Mr. Lekh Bahadur Thapa, Audit Officer
        '''
        dispatcher.utter_message(message)

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class RequirementQuestionnaire(Action):
    def name(self) -> Text:
        return "action_requirement_questionnaire"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        message = '''
        Some of the important questions that were asked during the survey are: - \n
        •	Does annual planning team take any inputs from other directorates to prepare annual plan? If yes: which is the liaison body or directorate to coordinate the annual plan activities? \n
        •	In your view which process/ sub-process of audit lifecycle can be automated, where technology can be used? \n
        •	Who is responsible to define the category and give weightage of entities every year to define categories? \n
        •	Is there any identification code for each personnel of OAGN? If yes: what is the composition of the identification code? \n
        •	Does Audit Scope and List of Entities to be visited is provided to audit team, if yes in what format? \n
        •   Does different type of PACs have different roles and responsibilities?
        '''
        dispatcher.utter_message(message)

        # # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class ProposedFeatureNams(Action):
    def name(self) -> Text:
        return "action_proposed_features_nams"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        message = '''
        The salient features that has been proposed to NAMS are: - \n
        •	Entity Management \n
        •	Dynamic and Configurable Risk Engine \n
        •	Audit Planning \n
        •	Pre-Engagement and Team Planning \n
        •	Efficient PARA/Memo Management \n
        •	Checklist Management \n
        •	Reporting \n
        •	Follow up and Compliance \n
        •	Quality Control \n
        •	Monitoring and Control \n
        •	Quality Assurance \n
        •	Better communication and coordination \n
        •	MIS report generation \n
        •	Effective document Management \n
        •	Use of Data Analytics and AI
        '''
        dispatcher.utter_message(message)

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class FunctionalRequirement(Action):
    def name(self) -> Text:
        return "action_functional_requirement_nams"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        message = '''
        The functional requirements of this project are divided into 4 parts: - \n
        I.      Functionalities to capture and maintain data \n
        II.     List of third-party government information systems which will be integrated with NAMS for data exchange \n
        III.    Support Features \n
        IV.     Dashboards
        '''
        dispatcher.utter_message(message)

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class RequirementCaputreMaintainData(Action):
    def name(self) -> Text:
        return "action_requirement_caputre_maintain_data"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        message = '''
        The functional requirements of the functionality to capture and maintain data are: - \n
        a)	Audit Planning \n
        b)	Audit Execution & Reporting \n
        c)	Audit Follow-Up and Compliance (Auditee Response) \n
        d)	Quality Assurance \n
        e)	Grievance Management \n
        f)	Document Management \n
        g)	Legacy Data Capturing Module \n
        h)	Employee Data Management \n
        i)	Master Data Management \n
        j)	User Access Setting Management \n
        '''
        dispatcher.utter_message(message)

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class RequirementDashboard(Action):
    def name(self) -> Text:
        return "action_requirement_dashboard"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        message = '''
        The functional requirements of the Dashboard are: - \n
        a)	System Admin Dashboard \n
        b)	OAGN Level User Dashboard \n
        c)	External Auditor Dashboard \n
        d)	Auditee party/unit Dashboard \n
        e)	PAC Dashboard \n
        f)	QA/QC Dashboard \n
        '''
        dispatcher.utter_message(message)

        # # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class RequirementThirdParty(Action):
    def name(self) -> Text:
        return "action_requirement_third_party"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        message = '''
        The lists of third-party government information system which will be integrated with NAMS for data exchange are: - \n
        •	TSA, P-TSA \n
        •	RMIS \n
        •	LMBIS, P-LMBIS \n
        •	SUTRA \n
        •	PIS \n
        •	CGAS \n
        •	eGP \n
        •	DMS \n
        •	ITS \n
        •	ASYCUDA \n
        •	Data exchange with IDEA (NAMS to IDEA and IDEA to NAMS
        '''
        dispatcher.utter_message(message)

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class RequirementSupportFeatures(Action):
    def name(self) -> Text:
        return "action_requirement_support_features"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],) -> List[Dict[Text, Any]]:

        message = '''
        The support features of NAMS are: - \n
        •	Data Analytics & Artificial Intelligence Implementation \n
        •	Export of NAMS data to excel/ pdf/ csv format \n
        '''
        dispatcher.utter_message(message)

        # Saving logs to mongodb
        # user_id = tracker.sender_id
        # query = tracker.latest_message['text']
        # intent = tracker.latest_message['intent']
        # entities = tracker.latest_message['entities']
        # try:
        #     insert_logs(user_id, query, message, data=None,
        #                 intent=intent, entities=entities)
        # except Exception as error:
        #     print('Caught this error: ' + repr(error))
        #     pass

        return []


class ActionStoreLogs(Action):
    def name(self) -> Text:
        return "actionstorelogs"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:

        # with open('NAMS_logs.csv', 'w', newline='') as file:
        #     writer = csv.writer(file)
        #     writer.writerow(["User_ID", "Query", "Intent", "Intent Confidence",
        #                      "Entilties", "Reply", "Data", "Timestamp"])
        #     for document in collection.find():
        #         user_id = document['user_id']
        #         query = document['query']
        #         intent = document['intent']['name']
        #         intent_confidence = document['intent']['confidence']
        #         entities = document['entities']
        #         if len(entities) != 0:
        #             entity_list = []
        #             for ent in entities:
        #                 entity_list.append(ent['value'])
        #         else:
        #             entity_list = None
        #         reply = document['reply']
        #         data = document['data']
        #         if data != None:
        #             data_list = []
        #             for packages in data:
        #                 data_item = {
        #                     "id": packages['id'], "name": packages['name']}
        #                 data_list.append(data_item)
        #         else:
        #             data_list = None
        #         timestamp = document['timestamp']
        #         writer.writerow([user_id, query, intent, intent_confidence,
        #                          entity_list, reply, data_list, timestamp])
        dispatcher.utter_message(
            "Okay, I will store these chat logs with you.")
        return []
