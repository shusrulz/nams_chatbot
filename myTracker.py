from datetime import datetime
from pymongo import MongoClient
client = MongoClient()

client = MongoClient('mongodb://mongodb:27017/')

db = client['NAMS_logs']

collection = db['nams_collection']

now = datetime.now()


def insert_logs(user_id,query,reply,data,intent,entities):
    log = {"user_id": user_id,
            "query": query,
            "intent":intent,
            "entities":entities,
            "reply": reply,
            "data" : data,
            "timestamp": datetime.timestamp(now)
            }
    # posts = db.posts
    post_id = collection.insert_one(log)
    print(db.list_collection_names())
    return "Log Inserted!!"
