## intent:about_nams_bot

- What is this chatbot?
- chatbot
- about chatbot
- how can you support me
- What can you do?
- What services do you provide?
- What are you capable of?
- How can you help me?
- Tell me about yourself
- What do you do
- Tell me about your services
- Why are you built?
- What is your purpose
- I want to know about you
- Timi k ho?
- Timro kaam k ho
- Timi lay k garna sakxau
- Timi lai kina banaako
- Timro baare bhana ta
- Yo k ho
- What is this?
- Who made this chatbot?
- What can this chatbot do?
- How can this chatbot help?
- How can this chatbot be helpful?
- yo chatbot le k garxa ta?
- yo chatbot k ho?
- yo chatbot le k k garna sakxa ta
- yo chatbot k ko lagi ho
- yo chatbot nai ho ta
- yo chatbot ho ki manxe ho?
- kasle reply gardai xa?
- huh reply kasari aayo?
- khatra chatbot ho ta yo?
- yo system k ho
- what is this
- what is it?
- what is its purpose?
- how can you help?
- i need help
- can you help me?
- help
- k garxa yeslay
- yeslay k garxa

## intent:after_greet

- how are you?
- how are you doing
- are you fine?
- hello are you ok
- how is your condition?
- how have you been holding?
- namaste, kasto xa?
- what's up
- Hey buddy, what is up?
- Hey, what' up
- hey. how are you.
- hi. what's up
- hi how are you
- hello mate how are you
- K xa bro
- K xa hero
- k xa boro
- K xa
- Sanchai
- Sanchai xau
- Aramai
- Araamai xau bro
- K xa solti
- how are you

## intent:greeting

- Hello buddy
- Good morning
- hello friend
- hi
- Hey
- Good evening
- Hello bot
- Namaste
- namaskaar
- namaskar
- namaste
- ani k xa ta khabar?
- Pranipat chatbot shree

## intent:goodbye

- Goodbye
- Yes, thank you for helping me out.
- Thank you for helping me out
- Byebye
- Thank you for guiding me
- thank you
- Thanks for helping me out
- I am done.
- Thank you
- Bye
- Dhanyabaad
- Sahaoyog ko lagi dhanyabaad.
- This was helpful.
- This chatbot really helped. Thanks.
- Sahaoyog ko lagi dhanyabbad
- Ma gaye hai
- Aba jaanxu hai
- Ta ta
- tata
- ok

## intent:profane_words

- Fuck this chatbot.
- Fuck you.
- Esto mulla chatbot.
- Hawa jasto chatbot
- Ghanta jasto chatbot
- Khoya jasto chatbot
- This chatbot is shit.
- Useless fucking chatbot.
- Fuck
- Ghanta
- yo muji chatbot kei kaam xaina
- esto hawa chatbot banayera ta vayenani
- bhalu
- ta khatey hero bhako
- kutreya
- khopdi tod saaley ka
- bitch
- fuck you bitch
- ass
- what a shitty application

## intent:out_of_scope

- the earth is round
- who is the prime minister of nepal
- haha hahah hahah hahahaha
- kya jhyauu yar
- i am hungry
- lets have lunch
- lets go to watch movie
- film herna jaam
- bholi bida xa ke nai
- what is the weather today
- lets play some game
- go wash dishes
- the earth is flat i can prove it
- dance for me
- sing a song for me
- who is the hero in Titanic
- who sang the song dilbar dilbar
- who will win the premier league
- david beckham is very handsome
- do you want to fight with me?
- fight khelne ho single singele aija
- i want to know the president of USA
- give me 1 billion dollar
- what is info developer
- my teeth is paining
- my mobile device is not working
- the screen of my television is not working
- i want to eat
- what is the size of earth
- bla bla bla
- fsdfsdfsd dfdfd
- vsjdfklsdj kjfsldkfj
- fjskdjfkldfj
- who will be the next prime minister of nepal?

## intent:ask_definitions

- what is [Alphanumeric Data](definition)
- can you tell me about [Audit Area](definition)
- what do you mean by [Audit Execution Year](definition)
- explain [Audit Observation](definition)
- define [Audit Program](definition)
- i want to know what is [Auditable Year](definition)
- When you tell about [Authorized](definition) what do you mean
- Do you know about [Authorized](definition)
- i dont know about [Entity preparing financial statement](definition). Can you tell me what that is?
- umm, what is [Entity](definition)
- by any chance you can inform me regarding [Entity](definition)
- [Entity Owner Unit](definition) what is it?
- how much can you tell me about [Entity Owner Unit](definition)
- is it possible to explain to me regarding [Entrance Meeting](definition)?
- what is an [Entrance Meeting](definition)?
- can you enlighten me in the topic [Exit Meeting](definition)?
- what does [Exit Meeting](definition) mean?
- what do you mean by [Field Audit Unit](definition)
- define me [Field Audit Unit](definition)
- tell me about [Master Data Management](definition)
- share your thoughts about [Master Data Management](definition)
- hey there. tell me about what do you mean by [Numeric Data](definition)
- I wonder if you tell me what [Numeric Data](definition) is?
- hi. can you tell me what is [Preliminary Audit Report](definition)
- i would be obliged to know about [Preliminary Audit Report](definition)
- yo man, what do know about [Risk Grade of Entity](definition)
- would you be kind enough to give your view regarding [Risk Grade of Entity](definition)
- What is the meaning of [Role](definition)
- I am confused on the topic [Role](definition). Will you tell me about it?
- I don't know about [User](definition). Can you tell me what it means?
- share your idea on [User](definition)
- what is [master data management]{"entity": "definition", "value": "Master Data Management"}
- what does an [audit observation]{"entity": "definition", "value": "Audit Observation"} mean?
- tell me about [preliminary audit report]{"entity": "definition", "value": "Preliminary Audit Report"}?
- what does [entity]{"entity": "definition", "value": "Entity"} mean?
- [Entity](definition)? what does that mean?
- what is [exit meeting]{"entity": "definition", "value": "Exit Meeting"}

## intent:ask_abbreviations

- tell me the full form of [AAG](abbreviation)
- what does [AAG](abbreviation) stands for?
- full form of [AAP](abbreviation) please
- give me the full form of [AR](abbreviation)
- what do you mean by [AG](abbreviation)
- what is the meaning of [AI](abbreviation)
- [ASYCUDA](abbreviation) ? what does that mean?
- hey, what does [CA](abbreviation) mean
- give the full form of the word [CGAS](abbreviation)
- i want to know the full form of [CR](abbreviation)
- what is the meaning of [DAG](abbreviation)
- tell me about [DC](abbreviation)
- what do you mean by [DFR](abbreviation)
- what does [DMS](abbreviation) mean?
- hi, can you tell me what is [EA](abbreviation)
- give an information on [EAP](abbreviation)
- what is the full form of [EPFS](abbreviation)
- do you have any idea what does [FA](abbreviation) mean?
- what is the meaning of [FS](abbreviation)?
- full form of [FCGO](abbreviation)?
- meaning of [GoN](abbreviation)?
- what is [ITS](abbreviation)
- what is [LMBIS](abbreviation)
- what is the full form of the word [MAP](abbreviation)
- meaning of [NAMS](abbreviation)
- what does [NFRS](abbreviation) stands for
- [NGeA](abbreviation) stands for what
- [NLP](abbreviation) stands for what?
- [NPSAS](abbreviation) means?
- [OCR](abbreviation) meaning?
- [O&M](abbreviation)? what does that mean?
- what does [OAGN](abbreviation) stands for?
- what does [PA](abbreviation) mean?
- what is the meaning of [PAC](abbreviation)
- what is [PARF](abbreviation) ?
- what [PIS](abbreviation)
- tell me what is [QA](abbreviation)
- what do you mean by [QC](abbreviation)
- what does [RBAF](abbreviation) even mean
- what is [RMIS](abbreviation)?
- can you tell me the meaning of [SOE](abbreviation)
- give me the full form of [SUTRA](abbreviation)
- hey there buddy. what is [TL](abbreviation) ? can you tell
- i dont know about [TM](abbreviation). What does it mean?
- hi. i do not have any idea about whwat [TSA](abbreviation) means. can you help me with it?
- what is the full form of [PAR](abbreviation)
- what is [RMIS](abbreviation)
- what do you mean by [QA](abbreviation)
- full form of [NAMS](abbreviation)

## intent:project_background

- what is the background of this project
- tell me about the project
- what is the context of the project
- project background
- project's context
- give me the scenario of this project
- i want to know about the project background
- give me information on the background of this project
- any idea on the project background
- will you tell be about background of this project
- share your understanding on the background of this project
- let's have a talk on the background of this particular project
- i wanna discuss about this project
- explain me about this project
- is it possible to know about this project?
- would you kindly inform me about this project background?
- inform me about this project
- can i know about this project


## intent:project_expectation

- what are the expectations from this project
- what kind of result is OAGN expecting from this project
- can you say what to expect from this project at the end
- what to expect from this project
- what is OAGN expecting from this project
- what are the new things that we can hope to get from this project

## intent:feature_da_nams

- what features and functionalities are found from the report of diagnostic assessment of NAMS
- features and functionalities found based on the study of COTS
- what features were discovered when doing the diagnostic assessment of NAMS
- report of diagnostic assessment of nams
- what are the features that were found from the diagnostic assessment of nams

## intent:committee_members

- who were the members of the committee that was formed for requirement gathering of To-Be NAMS.
- give the name of the members allocated for To-Be NAMS
- members that were chosen by OAGN for gathering the requirements of to-be nams
- team members of to-be nams requirement gathering
- who were in the committee team that gathered the information of to-be nams
- who conducted the requirement gathering of To-Be NAMS

## intent:requirement_questionnaire

- what kinds of questions were prepared for the requirement gathering of To-Be NAMS
- types of questions that were asked during the requirement gathering process of To-Be NAMS
- list of questionnaire that were prepared by OAGN for to-be nams
- what kinds of questions were used during the requirement gathering process
- show the question samples which were used for questionnaire

## intent:proposed_features_nams

- what features has been proposed for NAMS
- what are the new features that will be used in NAMS
- new additional features to be implemented in NAMS project
- through this project, what new featuers are proposed for NASM
- in this project, what are the features which is going to be implemented in nams
- features that are going to be added in nams
- what are the features that were proposed for nams
- new additional feataures which are proposed for NAMS

## intent:functional_requirement_nams

- what are the functional requirements of NAMS
- name the functional requirements of nams
- funcational requirements that has been identified for this project
- list the functional requirement of this project
- show the functional requirements of nams
- what are the functional requirements of this application

## intent:requirement_caputre_maintain_data

- show the functionalities to capture and maintain data
- how will the data be captured and maintained in NAMS
- funcational requirements to capture the required data and maintain them
- what are the functional requirements of capturing and maintaining the data
- functional requirements for capturing and maintaing the data

## intent:requirement_dashboard

- functional requirements of dashboard
- what are the functional requirements that are to be noticed while creating dashboard
- what are the functional requirements for dashboard
- requirements of dashboard
- when creating the dashboard, what are the requirements that has to be fulfilled

## intent:requirement_third_party

- what are the third party system that will be used in NAMS
- list the third-party government systems which will be included in this NAMS project
- show the third party government information system which will be implemented in this project
- for the purpose of exchanging data what third party applications are being used
- what third party features are being used in nams
- in this NAMS system what other third party software are being used

## intent:requirement_support_features

- what are the supported features of this project
- support features of NAMS
- nams support features
- functional requirement of the support features
- what are the functional requirements of the support features of the NAMS application
- what are the support features that will be used in nams

## intent:store_data

- Please store the conversation
- store data
- save conversation logs
- save chat data
- store chat data
- save data
- store conversation data

## synonym:Alphanumeric Data

- alphanumeric data

## synonym:Audit Execution Year

- audit execution year

## synonym:Audit Observation

- audit observation

## synonym:Audit Program

- audit program

## synonym:Auditable Year

- auditable year

## synonym:Authorized

- authorized

## synonym:Entity

- entity

## synonym:Entity Onwer Unit

- entity owner unit

## synonym:Entity preparing financial statement

- entitiy preparing financial statement

## synonym:Entrance Meeting

- entrance meeting

## synonym:Exit Meeting

- exit meeting

## synonym:Field Audit Unit

- field audit unit

## synonym:Master Data Management

- master data management

## synonym:Numeric Data

- numeric data

## synonym:Preliminary Audit Report

- preliminary audit report

## synonym:Risk Grade of Entity

- risk grade of entitiy

## synonym:Role

- role

## synonym:User

- user
