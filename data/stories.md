## Intro
* greeting
  - action_greeting

## short convo
* greeting
  - action_greeting
* after_greet
  - action_after_greet
* goodbye
  - action_goodbye
  - action_restart

## Goodbye
* goodbye
  - action_goodbye
  - action_restart

## Save_data
* store_data
  - actionstorelogs
  - action_restart

## interactive_story_1
* about_nams_bot
  - action_about_nams_bot

## interactive_story_2
* out_of_scope
  - action_out_of_scope

## interactive_story_3
* profane_words
  - action_profane_words

## interactive_story_1
* ask_abbreviations{"abbreviation": "RMIS"}
  - action_ask_abbreviations

## interactive_story_1
* ask_abbreviations{"abbreviation": "QA"}
  - action_ask_abbreviations

## interactive_story_1
* ask_abbreviations{"abbreviation": "DFR"}
  - action_ask_abbreviations

## interactive_story_1
* ask_abbreviations{"abbreviation": "EPFS"}
  - action_ask_abbreviations

## interactive_story_1
* ask_definitions{"definition": "Master Data Management"}
  - action_ask_definitions

## interactive_story_1
* ask_definitions{"definition": "Audit Observation"}
  - action_ask_definitions

## interactive_story_1
* ask_definitions{"definition": "Preliminary Audit Report"}
  - action_ask_definitions

## interactive_story_1
* ask_definitions{"definition": "Entity"}
  - action_ask_definitions

## interactive_story_1
* ask_definitions{"definition": "Entity"}
  - action_ask_definitions

## interactive_story_1
* ask_definitions{"definition": "Exit Meeting"}
  - action_ask_definitions

## interactive_story_1
* project_background
  - action_project_background

## interactive_story_2
* project_background
  - action_project_background

## interactive_story_3
* project_expectation
  - action_project_expectation

## interactive_story_4
* project_expectation
  - action_project_expectation

## interactive_story_5
* feature_da_nams
  - action_feature_da_nams

## interactive_story_6
* committee_members
  - action_committee_members

## interactive_story_7
* committee_members
  - action_committee_members

## interactive_story_8
* requirement_questionnaire
  - action_requirement_questionnaire

## interactive_story_9
* requirement_questionnaire
  - action_requirement_questionnaire

## interactive_story_10
* proposed_features_nams
  - action_proposed_features_nams

## interactive_story_11
* proposed_features_nams
  - action_proposed_features_nams

## interactive_story_12
* functional_requirement_nams
  - action_functional_requirement_nams

## interactive_story_13
* functional_requirement_nams
  - action_functional_requirement_nams

## interactive_story_14
* requirement_caputre_maintain_data
  - action_requirement_caputre_maintain_data

## interactive_story_15
* requirement_dashboard
  - action_requirement_dashboard

## interactive_story_16
* requirement_dashboard
  - action_requirement_dashboard

## interactive_story_17
* requirement_third_party
  - action_requirement_third_party

## interactive_story_18
* requirement_third_party
  - action_requirement_third_party

## interactive_story_19
* requirement_support_features
  - action_requirement_support_features
